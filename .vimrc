" Enable modern Vim features not compatible with Vi spec.
set nocompatible

" All of your plugins must be added before the following line.

" Enable file type based indent configuration and syntax highlighting.
filetype plugin indent on
syntax on

set nu
set undofile undodir=~/.vim/undodir
set tabstop=2
set shiftwidth=2
set expandtab

autocmd BufWritePre * %s/\s\+$//e " Strip trailing whitespace on save.
set pastetoggle=<F3>

color darkblue

" Highlight line in insert mode.
autocmd InsertEnter,InsertLeave * set cul!
hi CursorLine cterm=none ctermbg=234

