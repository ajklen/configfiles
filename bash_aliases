alias stat="stat -x"
alias grep="grep --color=auto"
alias json="python -m json.tool"
mkcd ()
{
  mkdir -p -- "$1" &&
    cd -P -- "$1"
}

alias octave="octave --no-gui"
alias py3="python3"

alias setclip="xclip -selection c"
alias getclip="xclip -selection clipboard -o"

